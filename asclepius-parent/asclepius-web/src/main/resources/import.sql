insert into patients(id, first_name, last_name) values (1, 'Adam', 'Todosić');
insert into patients(id, first_name, last_name) values (2, 'Radoman', 'Magazinović');
insert into patients(id, first_name, last_name) values (3, 'Vladislav', 'Bandić');
insert into patients(id, first_name, last_name) values (4, 'Mihajlo', 'Maljević');
insert into patients(id, first_name, last_name) values (5, 'Vidoje', 'Celiković');
insert into patients(id, first_name, last_name) values (6, 'Jugoslav', 'Crvenković');
insert into patients(id, first_name, last_name) values (7, 'Nebojša', 'Branovacki');
insert into patients(id, first_name, last_name) values (8, 'Stracimir', 'Vuksić');
insert into patients(id, first_name, last_name) values (9, 'Neven', 'Milojkić');
insert into patients(id, first_name, last_name) values (10, 'Vladislav', 'Rancić');
insert into patients(id, first_name, last_name) values (11, 'Veselinka', 'Joveljić');
insert into patients(id, first_name, last_name) values (12, 'Renja', 'Jevcić');
insert into patients(id, first_name, last_name) values (13, 'Nevena', 'Pjević');
insert into patients(id, first_name, last_name) values (14, 'Nataša', 'Vukadin');
insert into patients(id, first_name, last_name) values (15, 'Danka', 'Przica ');
insert into patients(id, first_name, last_name) values (16, 'Daliborka', 'Bumb');
insert into patients(id, first_name, last_name) values (17, 'Bobana', 'Svircev');
insert into patients(id, first_name, last_name) values (18, 'Marina', 'Noković');
insert into patients(id, first_name, last_name) values (19, 'Anka', 'Anastasijević');
insert into patients(id, first_name, last_name) values (20, 'Dušanka', 'Reković');
insert into patients(id, first_name, last_name) values (21, 'Srđan', 'Sevo');
insert into patients(id, first_name, last_name) values (22, 'Ilija', 'Boranjac');
insert into patients(id, first_name, last_name) values (23, 'Gabrijel', 'Janasković');
insert into patients(id, first_name, last_name) values (24, 'Obren', 'Milicević');
insert into patients(id, first_name, last_name) values (25, 'Slaven', 'Rajaković');
insert into patients(id, first_name, last_name) values (26, 'Vukajlo', 'Bjelovuk');
insert into patients(id, first_name, last_name) values (27, 'Jovan', 'Cukuljić');
insert into patients(id, first_name, last_name) values (28, 'Momčilo', 'Zablacanski');
insert into patients(id, first_name, last_name) values (29, 'Dobroslav', 'Ivaz ');
insert into patients(id, first_name, last_name) values (30, 'Ljubivoje', 'Sovilj');
insert into patients(id, first_name, last_name) values (31, 'Desanka', 'Stanicić');
insert into patients(id, first_name, last_name) values (32, 'Maja', 'Poljanski ');
insert into patients(id, first_name, last_name) values (33, 'Neda', 'Senić');
insert into patients(id, first_name, last_name) values (34, 'Ksenija', 'Rajkov');
insert into patients(id, first_name, last_name) values (35, 'Slava', 'Klacar');
insert into patients(id, first_name, last_name) values (36, 'Radana', 'Colić');
insert into patients(id, first_name, last_name) values (37, 'Marina', 'Mrkela');
insert into patients(id, first_name, last_name) values (38, 'Stojanka', 'Mirović');
insert into patients(id, first_name, last_name) values (39, 'Dušica', 'Macut');
insert into patients(id, first_name, last_name) values (40, 'Branka', 'Cikota');