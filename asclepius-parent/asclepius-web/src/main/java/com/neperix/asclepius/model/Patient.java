package com.neperix.asclepius.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "patients")
public class Patient {

	@Id
	@GeneratedValue(generator = "id-generator")
	@GenericGenerator(name = "id-generator", strategy = "sequence")
	@Column(name = "id", unique = true)
	private int id;
	
	@Column(name = "first_name", length = 15)
	private String firstName;

	@Column(name = "last_name", length = 15)
	private String lastName;

	public Patient(int id, String first, String last) {
		this.id = id;
		this.firstName = first;
		this.lastName = last;
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}
	
	Patient() {
		// needed for Hibernate
	}
}
