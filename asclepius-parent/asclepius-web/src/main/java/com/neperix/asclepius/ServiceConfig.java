package com.neperix.asclepius;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.neperix.asclepius.model.PatientRepository;
import com.neperix.asclepius.model.PatientServiceImpl;

@Configuration
public class ServiceConfig {

	@Bean
	public PatientService patientService(PatientRepository patientRepository) {
		return new PatientServiceImpl(patientRepository);
	}
}
