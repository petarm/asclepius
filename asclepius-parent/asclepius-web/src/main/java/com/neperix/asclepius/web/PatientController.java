package com.neperix.asclepius.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.neperix.asclepius.PatientService;
import com.neperix.asclepius.model.Patient;

@RestController
@RequestMapping("/patients")
public class PatientController {

	@Autowired
	private PatientService patientService;
	
	@RequestMapping("/all")
	public List<Patient> all() {
		return this.patientService.getAll();
	}
	
	@RequestMapping("/{id}")
	public Patient byId(@PathVariable String id) {
		return new Patient(1, "Petar", "Mitrovic");
	}
	
}
