package com.neperix.asclepius.model;

public class MedicalRecord {

	private final int id;

	private final String diagnosys;

	private final String therapy;

	private final String entry;

	public MedicalRecord(int id, String diagnosys, String therapy, String entry) {
		this.id = id;
		this.diagnosys = diagnosys;
		this.therapy = therapy;
		this.entry = entry;
	}

	public int getId() {
		return id;
	}

	public String getDiagnosys() {
		return diagnosys;
	}

	public String getTherapy() {
		return therapy;
	}

	public String getEntry() {
		return entry;
	}
}
