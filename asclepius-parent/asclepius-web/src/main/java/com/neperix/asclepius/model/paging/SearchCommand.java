package com.neperix.asclepius.model.paging;

public interface SearchCommand {

	public static enum Direction {
		ASC,
		DESC
	}
	
	int offset();
	int limit();
	
	String[] projections();
	Direction direction();
}
