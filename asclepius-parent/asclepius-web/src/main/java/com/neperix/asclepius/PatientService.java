package com.neperix.asclepius;

import java.util.List;

import com.neperix.asclepius.model.Patient;

public interface PatientService {

	List<Patient> getAll();
}
