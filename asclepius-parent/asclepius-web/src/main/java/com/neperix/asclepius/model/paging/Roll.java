package com.neperix.asclepius.model.paging;

import java.util.Collection;

public class Roll<T> {

	private final Collection<T> data;
	private final int total;
	
	public Roll(Collection<T> data, int total) {
		super();
		this.data = data;
		this.total = total;
	}

	public Collection<T> getData() {
		return data;
	}

	public int getTotal() {
		return total;
	}
}
