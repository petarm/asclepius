package com.neperix.asclepius.model;

import java.util.List;

import org.springframework.data.domain.PageRequest;

import com.google.common.collect.Lists;
import com.neperix.asclepius.PatientService;
import com.neperix.asclepius.model.paging.SearchCommand;

public class PatientServiceImpl implements PatientService {

	private final PatientRepository patientRepository;

	public PatientServiceImpl(PatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}
	
	public List<Patient> getAll() {
		return Lists.newArrayList(this.patientRepository.findAll());
	}
	
	public List<Patient> getAll(SearchCommand command) {
		return Lists.newArrayList(
				this.patientRepository.findAll(new PageRequest(command.offset(), command.limit())));
	}

}
