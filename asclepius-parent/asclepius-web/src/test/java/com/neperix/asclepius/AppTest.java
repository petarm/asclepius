package com.neperix.asclepius;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.neperix.asclepius.model.PatientRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
public class AppTest {

	@Autowired
	private PatientRepository patientRepository;
	
	@Test
	public void test() {
		this.patientRepository.findAll();
	}
}
