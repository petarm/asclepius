# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

We're using Dockerfile located at asclepius-application module.

Build a docker image:
```
#!Shell
docker build -t petarmitrovic/asclepius .
```

Login to docker hub:
```
#!Shell
docker login --username=petarmitrovic --email=petarmitrovic@gmail.com
```

Deploying the image:

```
#!Shell
docker push petarmitrovic/asclepius
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact